package ru.kav.file;
import java.io.*;
/**
 * Created by user on 31.05.2017.
 */
public class Intdata {
    public static void main(String[] args) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new FileReader("inputNum.txt"));
        DataOutputStream dataOutputStream = new DataOutputStream(new FileOutputStream("intdata.dat"));
        String string;
        while ((string= bufferedReader.readLine()) != null) {
            dataOutputStream.writeInt(Integer.parseInt(string));
        }
        bufferedReader.close();
        dataOutputStream.close();
    }
}
