package ru.kav.file;

import java.io.*;

/**
 * Created by user on 31.05.2017.
 */
public class Int6data {
    public static void main(String[] args) throws IOException {
        DataInputStream num = new DataInputStream(new FileInputStream("intdata.dat"));
        DataOutputStream num2 = new DataOutputStream(new FileOutputStream("int6data.dat"));
           try {
               while (true) {
                   int num1 = num.readInt();
                   if (num1 > 99999 && num1 < 1000000) {
                       num2.writeInt(num1);
                       System.out.println(num1);
                   }
               }
           }catch (EOFException a) {
                num.close();
                num2.close();
            }


        }
    }
