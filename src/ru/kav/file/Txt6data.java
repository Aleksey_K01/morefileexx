package ru.kav.file;
import java.io.*;
/**
 * Created by user on 06.06.2017.
 */
public class Txt6data {
    public static void main(String[] args) throws IOException {
        DataInputStream dataInputStream = new DataInputStream(new FileInputStream("int6data.dat"));
        DataOutputStream dataOutputStream = new DataOutputStream(new FileOutputStream("txt6data.dat"));
        try {
            while (dataInputStream.available() != 0) {
                int num = dataInputStream.readInt();
                if (isHappy(num))
                    dataOutputStream.writeChars(num + "\n");
            }
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        } finally {
            dataOutputStream.close();
            dataInputStream.close();
        }
    }



    private static boolean isHappy(Integer num) {
        int a = num % 10;
        num = num / 10;
        int b = num % 10;
        num = num / 10;
        int c = num % 10;
        num = num / 10;
        int d = num % 10;
        num = num / 10;
        int e = num % 10;
        num = num / 10;
        int f = num % 10;
        num = num / 10;
        int sum = a + b + c;
        int sum2 = d + e + f;
        return sum == sum2;
    }
}